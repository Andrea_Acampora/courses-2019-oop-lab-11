﻿using System;
using System.Runtime.CompilerServices;

namespace Cards
{
    class Deck
    {
        private Card[] cards;

        public Card this[ItalianSeed seed, ItalianValue value]
        {
            get
            {
                foreach (Card c in cards)
                {
                   
                    if (int.Parse(c.Seed) == (int)seed &&  int.Parse(c.Value)== (int)value )
                    {
                        return c;
                    }
                }

                return null;
            }
        }

        public Deck()
        {
        }

        public void Initialize()
        {
            /*
             * == README ==
             * 
             * I due enumerati (ItalianSeed e ItalianValue) definiti in fondo a questo file rappresentano rispettivamente semi e valori
             * di un tipo mazzo di carte da gioco italiano.
             * 
             * Questo metodo deve inizializzare il mazzo (l'array cards) considerando tutte le combinazioni possibili.
             * 
             * Nota - Dato un generico enumerato MyEnum:
             *   a) è possibile ottenere il numero dei valori presenti con Enum.GetNames(typeof(MyEnum)).Length
             *   b) è possibile ottenere l'elenco dei valori presenti con (MyEnum[])Enum.GetValues(typeof(MyEnum))
             * 
             */
            this.cards = new Card[40];
            int i = 0;

            foreach (int seed in (ItalianSeed[]) Enum.GetValues(typeof(ItalianSeed)))
            {
                foreach (int value in (ItalianValue[]) Enum.GetValues(typeof(ItalianValue)))
                {
                    cards[i] = new Card(value.ToString(), seed.ToString());
                    i++;
                }
            }
        }

        public void Print()
        {
            /*
             * == README  ==
             * 
             * Questo metodo stampa tutte le carte presenti nel mazzo, con una propria rappresentazione a scelta.
             */
            //throw new NotImplementedException();
            foreach (Card c in cards)
            {
                Console.WriteLine(c.ToString());
            }
        }

    }

     enum ItalianSeed
        {
            DENARI,
            COPPE,
            SPADE,
            BASTONI
        }

        enum ItalianValue
        {
            ASSO,
            DUE,
            TRE,
            QUATTRO,
            CINQUE,
            SEI,
            SETTE,
            FANTE,
            CAVALLO,
            RE
        }
    
}